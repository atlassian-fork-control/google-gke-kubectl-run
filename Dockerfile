FROM google/cloud-sdk:273.0.0-slim

COPY requirements.txt /

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update -qqy && \
    apt-get install --no-install-recommends -qqy \
        apt-transport-https=1.8.2 \
        python3-pip=18.1-5 \
        python3-setuptools=40.8.0-1 \
        kubectl=1.17.3-00 && \
    pip3 --no-cache-dir install -r requirements.txt && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

ENTRYPOINT ["python3", "/pipe.py"]
